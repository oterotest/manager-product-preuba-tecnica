package com.product.apirest.interfaces.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.product.apirest.interfaces.IServiceProductImpl;
import com.product.apirest.model.Product;


@RunWith(MockitoJUnitRunner.class)
public class IServiceProductImpltest {

	
	@InjectMocks
	IServiceProductImpl productInjectMock;
	
	@Mock
	IServiceProductImpl producttMock;

	@Test
	
	public void uploadImportCSVTestRutaNoExits() {
		when(productInjectMock.uploadCSV("...cargue.cvs")).thenReturn(new ArrayList<Product>());
		List<Product> productList1=productInjectMock.uploadCSV("cargue.cvs");
		assertEquals(new ArrayList<Product>(), productList1);
		
		
	}
	
	public void uploadImportCSVTestRutaPerfect() {
		when(productInjectMock.uploadCSV("cargue.cvs")).thenReturn(new ArrayList<Product>());
		List<Product> productList1=productInjectMock.uploadCSV("cargue.cvs");
		assertEquals(new ArrayList<Product>(), productList1);
		
		
	}
	
	
}
