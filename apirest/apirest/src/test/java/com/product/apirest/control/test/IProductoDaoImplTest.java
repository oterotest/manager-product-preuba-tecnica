package com.product.apirest.control.test;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.product.apirest.control.*;

import com.product.apirest.model.Product;

import org.junit.Assert;

import java.sql.SQLException;
import java.time.LocalDate;

@RunWith(MockitoJUnitRunner.class)
public class IProductoDaoImplTest {

	@Mock
	IProductDaoImpl productImplMock;

	@InjectMocks
	IProductDaoImpl producInjectImplent;

	@Test
	void validateconexionDabaseProcessDeleteProductNotExist() throws SQLException {

		IProductDao db = new IProductDaoImpl();

		Assert.assertEquals(false,db.deleteProduct(7));

	}

	@Test
	void validateconexionDabaseProcessAlmacenarProductExist() throws SQLException {

		IProductDao db = new IProductDaoImpl();
		Product p = new Product();
		// p.setId(3);
		p.setCost(100);
		p.setName("Name");
		p.setDateEnable(LocalDate.now());
		p.setDateLoad(LocalDate.now());
		p.setQuantity(121);
		p.setType(4);

		double commission = (p.getType() == 1) ? p.getCost() * 0.12 : 0;
		commission += (p.getType() == 2) ? p.getCost() * 0.305 : 0;
		commission += (p.getType() == 3) ? p.getCost() * 0.0895 : 0;
		commission += (p.getType() == 4) ? p.getCost() * 0.1033 : 0;
		commission += p.getCost() * 0.19;
		p.setPrice(commission);
		Assert.assertEquals(true, db.saveProduct(p));

	}

	@Test
	void validatecalculocomision() throws SQLException {

		Product p = new Product();
		p.setCost(100);
		p.setType(4);

		double commission = (p.getType() == 1) ? p.getCost() * 0.12 : 0;
		commission += (p.getType() == 2) ? p.getCost() * 0.305 : 0;
		commission += (p.getType() == 3) ? p.getCost() * 0.0895 : 0;
		commission += (p.getType() == 4) ? p.getCost() * 0.1033 : 0;
		commission += p.getCost() * 0.19;
		p.setPrice(commission);
		Assert.assertEquals(29.33d, commission, 0.9);

	}

	@Test
	void validateconexionDabaseProcessAlamacenaProductSinData() throws SQLException {

		IProductDao db = new IProductDaoImpl();
		Product p = new Product();

		Assert.assertEquals(db.saveProduct(null), p);

	}

	@Test
	void countProductSaveDataBase() throws SQLException {

		IProductDao db = new IProductDaoImpl();
		Assert.assertEquals(8, db.listProduct().size());
	}
}
