package com.product.apirest.interfaces;

import java.io.FileReader;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csvreader.CsvReader;
import com.product.apirest.control.IProductDao;
import com.product.apirest.model.Inform;
import com.product.apirest.model.Product;

@Service
public class IServiceProductImpl implements IServiceProduct {

	private List<Inform> listCase = new ArrayList<>();
 
	@Autowired
	IProductDao dao;

	@Autowired
	MailServiceSend serviceMail;
	

	@Override
	public List<Product> listProduct() throws SQLException {
		return dao.listProduct();
	}

	
	@Override
	public List<Product> uploadCSV(String path) {
		listCase = new ArrayList<>();
		List<Product> listImport = new ArrayList<>();
		int line = 0;
		try {
			CsvReader readProduct = new CsvReader(new FileReader(path));
			readProduct.readHeaders();
			while (readProduct.readRecord()) {
				line++;
				Inform info = new Inform();
				transProductLineCVS(readProduct,  listImport, info, line);
			}
		} catch (Exception e) {
			Logger.getGlobal().warning(e.getMessage());
		}
		return listImport;
	}

	public void transProductLineCVS(CsvReader readProduct, List<Product> listImport, Inform info,
			int line) {
		 
		try {
			Product p = new Product();
			p.setName(readProduct.get(0));
			p.setCost(Double.parseDouble(readProduct.get(1)));
			p.setType(Integer.parseInt(readProduct.get(2)));
			p.setDateEnable(LocalDate.parse(readProduct.get(3)));
			p.setQuantity(Integer.parseInt(readProduct.get(4)));
			p.setDateLoad(LocalDate.now());
			p.setPrice(calculPrice(p.getType(), p.getCost()));
			listImport.add(p);
			info.setStatusCorrect(true);
		} catch (Exception e) {
			info.setStatusCorrect(false);
			info.setComment(e.getMessage());
		}
		info.setLine(line);
		listCase.add(info);
	}

	@Override
	public String insertProduct(List<Product> list) throws SQLException {
		
		list.forEach(x -> {
			try {
				dao.saveProduct(x);
			} catch (SQLException e) {
				Logger.getGlobal().warning(e.getMessage());
			}
		});
		
		return "OK";
	}

	@Override
	public String updateProduct(Product obj) throws SQLException {
		obj.setPrice(calculPrice(obj.getType(), obj.getCost()));
		return "R"+dao.updateProduct(obj.getId(), obj);
	}


	public double calculPrice(int type, double cost) {
		double commission = (type == 1) ? cost * 0.12 : 0;
		commission += (type == 2) ? cost * 0.305 : 0;
		commission += (type == 3) ? cost * 0.0895 : 0;
		commission += (type == 4) ? cost * 0.1033 : 0;
		commission += cost * 0.19;
		return commission;
	}

	@Override
	public String deleteProduct(int id) throws SQLException {
		dao.deleteProduct(id);
		return "OK";
	}


	@Override
	public List<Inform> getInfoms() {
		
		return listCase;
	}

}
