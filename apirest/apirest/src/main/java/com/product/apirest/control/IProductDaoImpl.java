package com.product.apirest.control;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.springframework.stereotype.Component;

import com.product.apirest.interfaces.Conexion;
import com.product.apirest.model.Product;

@Component
public class IProductDaoImpl implements IProductDao {
	Conexion cx = new Conexion();
	@Override
	public ArrayList<Product> listProduct() throws SQLException {
		ArrayList<Product> listFind = new ArrayList<>();
		try (PreparedStatement preStament = cx.conection()
				.prepareStatement("SELECT id,type,price,dateload,cost,dateenable,name,quantity FROM product ")) {
			try (ResultSet rsSet = preStament.executeQuery()) {
				while (rsSet.next()) {
					listFind.add(transData(rsSet));

				}
			}

		}
		cx.closeConx();
		return listFind;
	}

	public Product transData(ResultSet rsSet) throws SQLException {
		Product objectPr = new Product();
		objectPr.setId(rsSet.getLong(1));
		objectPr.setType(rsSet.getByte(2));
		objectPr.setPrice(rsSet.getDouble(3));
		objectPr.setDateLoad(rsSet.getDate(4).toLocalDate());
		objectPr.setCost(rsSet.getDouble(5));
		objectPr.setDateEnable(rsSet.getDate(6).toLocalDate());
		objectPr.setName(rsSet.getString(7));
		objectPr.setQuantity(rsSet.getInt(8));
		return objectPr;
	}

	@Override
	public boolean saveProduct(Product product) throws SQLException {

		try (PreparedStatement preStament = cx.conection().prepareStatement(
				"INSERT INTO product (id,type,price,dateLoad,cost,dateEnable,name,quantity) VALUES (?,?,?,?,?,?,?,?)")) {
			preStament.setLong(1, product.getId());
			preStament.setInt(2, product.getType());
			preStament.setDouble(3, product.getPrice());
			preStament.setDate(4, Date.valueOf(product.getDateLoad()));
			preStament.setDouble(5, product.getCost());
			preStament.setDate(6, Date.valueOf(product.getDateEnable()));
			preStament.setString(7, product.getName());
			preStament.setInt(8, product.getQuantity());
			preStament.executeUpdate();
			cx.closeConx();
			return true;
		}
		
	}

	@Override
	public boolean updateProduct(long id, Product p) throws SQLException{

		try (PreparedStatement preStament = cx.conection()
				.prepareStatement("UPDATE product SET cost=?,quantity=?, name=?, type=? WHERE id=?")) {
			preStament.setDouble(1, p.getCost());
			preStament.setInt(2, p.getQuantity());
			preStament.setString(3, p.getName());
			preStament.setInt(4, p.getType());
			preStament.setLong(5, p.getId());
			preStament.execute();
			cx.closeConx();
			return true;
		}
		
		
	}

	@Override
	public boolean deleteProduct(long id) throws SQLException{
		try (PreparedStatement preStament = cx.conection().prepareStatement(
				"DELETE FROM product WHERE id=?")) {
			preStament.setLong(1, id);
			preStament.executeUpdate();
			cx.closeConx();
			return true;
		}}

	@Override
	public Product findIdProduct(long id)throws SQLException {
		Product product=new Product();
		try (PreparedStatement preStament = cx.conection()
				.prepareStatement("SELECT id,type,price,dateload,cost,dateenable,name FROM product WHERE id=?")) {
			try (ResultSet rsSet = preStament.executeQuery()) {
				while (rsSet.next()) {
					product=transData(rsSet);

				}
			}

		}
		cx.closeConx();
		return product;
	}

}
