package com.product.apirest.model;

import java.io.Serializable;

public class Inform implements Serializable{


	private static final long serialVersionUID = 1L;
	private int line;
	private boolean statusCorrect;
	private String comment;

	public int getLine() {
		return line;
	}

	public boolean isStatusCorrect() {
		return statusCorrect;
	}

	public String getComment() {
		return comment;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public void setStatusCorrect(boolean statusCorrect) {
		this.statusCorrect = statusCorrect;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "Inform [line=" + line + ", status=" + (statusCorrect?"CORRECT":"FAIL") + ", comment=" + (comment==null?"Not Comment":comment) + "]";
	}

}
