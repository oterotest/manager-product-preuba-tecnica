package com.product.apirest.interfaces;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class Conexion {

	private String driver = "com.mysql.cj.jdbc.Driver";
	private String url = "jdbc:mysql://localhost:3306/oterotesproduct?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private String username = "oterotes_camion";
	private String pass = "C4mionC4mion";
	private Connection conectionDatabase = null;
	public String getPass() {
		return pass;
	}

	public Connection conection() {

		try {
			Class.forName(driver);
			conectionDatabase =  DriverManager.getConnection(url, username, getPass());
		} catch (Exception e) {
			Logger.getGlobal().warning(e.getMessage());
		}
		return conectionDatabase;
	}

	public void closeConx() throws SQLException {
		if(conectionDatabase!=null) {
			conectionDatabase.close();
		}
	}
}
