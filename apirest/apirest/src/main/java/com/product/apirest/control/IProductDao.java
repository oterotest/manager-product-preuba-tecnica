package com.product.apirest.control;

import java.sql.SQLException;
import java.util.List;

import com.product.apirest.model.Product;

public interface IProductDao {
	
	public List<Product> listProduct() throws SQLException;
	
	public boolean saveProduct(Product product) throws SQLException ;
	
	public boolean updateProduct(long id, Product p) throws SQLException;
	
	public boolean deleteProduct(long id) throws SQLException;
	
	public Product findIdProduct(long id) throws SQLException;

}
