package com.product.apirest.interfaces;

import java.sql.SQLException;
import java.util.List;

import com.product.apirest.model.Inform;
import com.product.apirest.model.Product;

public interface IServiceProduct {

	public List<Product> listProduct() throws SQLException;


	public List<Product> uploadCSV(String path);

	public String insertProduct(List<Product> list) throws SQLException;

	public String updateProduct(Product obj) throws SQLException;

	public String deleteProduct(int id) throws SQLException;
	
	public  List<Inform> getInfoms();
	
}
