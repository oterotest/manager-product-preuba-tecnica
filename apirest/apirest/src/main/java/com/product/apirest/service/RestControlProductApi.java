package com.product.apirest.service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.product.apirest.interfaces.IServiceProduct;
import com.product.apirest.interfaces.MailServiceSend;
import com.product.apirest.model.Product;

@CrossOrigin(origins = { "http://localhost:3000" })
@RestController
public class RestControlProductApi {

	@Autowired
	IServiceProduct service;

	@GetMapping("/all")
	public List<Product> list() throws SQLException {
		return service.listProduct();
	}

@Autowired
MailServiceSend  send;

	@ResponseBody
	@PostMapping("/uploadfile")
	public String uploadFile(@RequestParam("file") MultipartFile mpf, String mail) {
		String path = "cargue.csv";
		try {
			File convertFile = new File(path);
			try (FileOutputStream fileout = new FileOutputStream(convertFile)) {
				fileout.write(mpf.getBytes());
			}
			if (convertFile.isFile()) {
				service.insertProduct(service.uploadCSV(path));
			}
			send.sendMail( mail, "Inform Product", service.getInfoms());
		} catch (Exception e) {
			Logger.getGlobal().warning(e.getMessage());
			return "NO";
		}
		return "OK";

	}

	@DeleteMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		try {
			service.deleteProduct(id);
		} catch (SQLException e) {
			return "NO" + e.getMessage();
		}

		return "OK";

	}

	@PutMapping("/put")
	@ResponseBody
	public String update(@RequestParam("id") int id, @RequestParam("name") String name,
			@RequestParam("cost") double cost, @RequestParam("quantity") int quantity, @RequestParam("type") int type) {
		try {
			Product p = new Product();
			p.setCost(cost);
			p.setName(name);
			p.setQuantity(quantity);
			p.setId(id);
			p.setType(type);
			service.updateProduct(p);
		} catch (SQLException e) {
			return "NO";
		}

		return "OK";

	}

}
