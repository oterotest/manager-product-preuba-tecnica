package com.product.apirest.interfaces;


import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.product.apirest.model.Inform;

@Service
public class MailServiceSend {

	@Autowired
	private JavaMailSender sendMail;

	public String sendMail(String to, String subject, List<Inform> listCase) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setSubject(subject+" "+LocalDateTime.now());
		StringBuilder out =new StringBuilder();
		out.append("Sum Correct : "+listCase.stream().filter(x->x.toString().contains("status=CORRECT,")).count());
		out.append("\nSum Fail : "+listCase.stream().filter(x->x.toString().contains("status=FAIL,")).count()+"\n");
		listCase.forEach(x -> out.append("\n" + x));
		message.setText(out.toString());
		sendMail.send(message);
		return "OK";
	}
}
