package com.product.apirest.model;


import java.io.Serializable;
import java.time.LocalDate;

public class Product implements Serializable{


	public Product() {
		super();
	}

	public Product(String name, double cost, int type, LocalDate dateEnable, 
			int quantity) {
		super();
		this.name = name;
		this.cost = cost;
		this.type = type;
		this.dateEnable = dateEnable;
		this.quantity = quantity;
	}

	private static final long serialVersionUID = 1L;

	private long id;

	private String name;

	private double cost;

	private double price;

	private int type;

	private LocalDate dateEnable;

	private LocalDate dateLoad;

	private int quantity;
	
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public long getId() {
		return id;
	}
    
	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public LocalDate getDateEnable() {
		return dateEnable;
	}

	public void setDateEnable(LocalDate dateEnable) {
		this.dateEnable = dateEnable;
	}

	public LocalDate getDateLoad() {
		return dateLoad;
	}

	public void setDateLoad(LocalDate dateLoad) {
		this.dateLoad = dateLoad;
	}

}
