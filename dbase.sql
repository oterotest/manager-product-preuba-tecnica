SELECT * FROM oterotesproduct.product;


CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` int NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `dateload` date NOT NULL,
  `cost` double NOT NULL DEFAULT '0',
  `dateenable` date NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9507 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci