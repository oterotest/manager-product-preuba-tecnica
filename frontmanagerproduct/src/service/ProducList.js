import React from "react";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import axios from "axios";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Dropdown } from "primereact/dropdown";

export default class ProductList extends React.Component {
  constructor() {
    super();
    this.state = { products: [], product: {} };
  }

  updateProperty(property, value) {
    let product = this.state.product;
    product[property] = value;
    this.setState({ product: product });
  }

  componentDidMount = () => {
    axios.get(`http://localhost:8080/all`).then(res => {
      const products = res.data;
      this.setState({ products });
    });
  };

  delete = () => {
    axios
      .delete(
        `http://localhost:8080/delete?id=${this.state.selectedProduct.id}`
      )
      .then(res => {
        if (res.status === 200) {
          this.componentDidMount();
        }
      });
    this.setState({ displayDialog: false });
  };

  update = () => {
    const data = new FormData();
    data.append("id", this.state.selectedProduct.id);
    data.append("name", this.state.product.name);
    data.append("cost", this.state.product.cost);
    data.append("quantity", this.state.product.quantity);
    data.append("type", this.state.product.type);

    axios
      .put("http://localhost:8080/put", data, {
        method: "PUT",
        body: data
      })
      .then(res => {
        this.componentDidMount();
      });
    this.setState({ displayDialog: false });
  };

  render() {
    const types = [
      { label: "type 1", value: "1" },
      { label: "type 2", value: "2" },
      { label: "type 3", value: "3" },
      { label: "type 4", value: "4" }
    ];
    return (
      <div>
        <div style={{ textAlign: "right" }}>
          <i className="pi pi-search" style={{ margin: "4px 4px 0 0" }}></i>
          <InputText
            type="search"
            onInput={e => this.setState({ globalFilter: e.target.value })}
            placeholder="Global Search"
            size="50"
          />
        </div>
        <DataTable
          value={this.state.products}
          paginator={true}
          rows={10}
          selectionMode="single"
          selection={this.state.selectedProduct}
          globalFilter={this.state.globalFilter}
          onSelectionChange={e =>
            this.setState({
              selectedProduct: e.value,
              displayDialog: true,
              product: e.value
            })
          }
          emptyMessage="No records found"
        >
          <Column field="id" header="Id" sortable={true} />
          <Column field="name" header="Name" sortable={true} />
          <Column field="cost" header="Cost" sortable={true} />
          <Column field="price" header="Price" sortable={true} />
          <Column field="quantity" header="Quantity" sortable={true} />
          <Column field="dateEnable" header="Date Enable" sortable={true} />
        </DataTable>
        <Dialog
          visible={this.state.displayDialog}
          style={{ width: "40vw" }}
          header="Product Details"
          modal={true}
          onHide={() =>
            this.setState({ displayDialog: false }, this.componentDidMount())
          }
        >
          {this.state.product && (
            <div className="p-grid">
              <div className="p-grid">
                <label className="p-col-2 styleetiq">Id </label>

                <InputText className="p-col-6" value={this.state.product.id} />
                <label className="p-col-2 styleetiq">Name </label>
                <InputText
                  value={this.state.product.name}
                  onChange={e => {
                    this.updateProperty("name", e.target.value);
                  }}
                />
              </div>
              <div className="p-grid">
                <label className="p-col-2 styleetiq">Cost </label>

                <InputText
                  value={this.state.product.cost}
                  onChange={e => {
                    this.updateProperty("cost", e.target.value);
                  }}
                />
                <label className="p-col-2 styleetiq">Type </label>

                <Dropdown
                  value={this.state.product.type}
                  placeholder={this.state.product.type}
                  options={types}
                  onChange={e => {
                    this.updateProperty("type", e.target.value);
                  }}
                />
              </div>
              <div className="p-grid">
              <label className="p-col-2 styleetiq">Quality</label>

              <InputText
                value={this.state.product.quantity}
                onChange={e => {
                  this.updateProperty("quantity", e.target.value);
                }}
              />
                              <label className="p-col-2 styleetiq">Price</label>

              
              <InputText value={this.state.product.price} /></div>
              <div className="p-grid">
              <label className="p-col-2 styleetiq">Date_load </label>

              
              <InputText
                value={this.state.product.dateLoad}
                onChange={e => {
                  this.updateProperty("dateload", e.target.value);
                }}
              />
                              <label className="p-col-2 styleetiq">Date_enable </label>

              <InputText
                value={this.state.product.dateEnable}
                onChange={e => {
                  this.updateProperty("dateenable", e.target.value);
                }}
              /></div>
            </div>
          )}
          <div>
            <Button
              label="Save Change"
              style={{ width: "10vw" }}
              icon="pi pi-check"
              onClick={this.update}
            />
            <Button
              label="Delete"
              icon="pi pi-times"
              style={{ width: "10vw" }}
              onClick={this.delete}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}
