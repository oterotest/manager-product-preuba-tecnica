import React, { Component } from "react";
import "./../App.css";
import { InputText } from "primereact/inputtext";
import axios from "axios";
class LoadFileApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      msg: ""
    };
  }
  change = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onClickHandler = e => {
    e.preventDefault();
    const data = new FormData();
    data.append("file", document.querySelector('input[type="file"]').files[0]);
    data.append("mail", this.state.email);
    axios
      .post("http://localhost:8080/uploadfile", data, {
        method: "POST",
        body: data
      })
      .then(function(respons) {
        if (respons.status === 200) {
          alert("Exit!! Send..");
          window.location.reload(true);
        } else if (respons.status === 401) {
          alert("Error !!");
        }
      });
  };
  render() {
    return (
      <form encType="multipart/form-data">
        <div className="styleAlingButtonFile">
          <input type="file" name="file" id="file"></input>
          <InputText
            name="email"
            placeholder="email notification"
            value={this.state.email}
            onChange={e => this.change(e)}
            required
            type="email"
          />
          <input
            type="submit"
            className="styleButton"
            value="send"
            onClick={this.onClickHandler}
          />
        </div>
      </form>
    );
  }
}

export default LoadFileApp;
