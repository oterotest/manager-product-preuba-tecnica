import React, { Component } from "react";
import "./../App.css";
import { AppBar, Tabs, Tab } from "@material-ui/core";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import TestDescApp from "./TestDescApp";
import MainApp from "./MainApp";

 function  a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    };
  }
  
class NavApp extends Component {

  about() {
    return <TestDescApp />;
  }

  dashboard() {
    return (
      <MainApp />
    );
  }

  render() {
    return (
      <Router>
        <div>
          <AppBar position="static" color="primary">
            <Tabs aria-label="simple tabs example">
              <Tab label="Dashboard" component={Link} to="/" {...a11yProps(0)} />
              <Tab label="About" component={Link} to="/about" {...a11yProps(1)}/>
            </Tabs>
          </AppBar>
          <Switch>
     
            <Route exact path="/" component={MainApp}>
              {this.dashboard}
            </Route>
            <Route path="/about">{this.about}</Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default NavApp;
