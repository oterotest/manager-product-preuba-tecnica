import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

import "./../App.css";
import {
  TableCell,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableContainer
} from "@material-ui/core";

class TestDescApp extends Component {
  createData(number, value) {
    return { number, value };
  }

  render() {
    const rows = [
      this.createData("1", "12%"),
      this.createData("2", "30.5%"),
      this.createData("3", "8.9%"),
      this.createData("4", "10.33%")
    ];
    return (
      <Card>
        <CardActionArea>
          <img src="text.jpg" alt="Komet Sales" className="space" />
          <CardContent className="styleAlingLeft">
            <Typography gutterBottom variant="h3" component="h2">
              Technique Test
            </Typography>
            <Typography
              variant="body1"
              color="textSecondary"
              component="p"
              className="styleMarginButtom"
            >
              Crear una aplicación java web que permita hacer manejo de
              inventario de productos de su preferencia, se deben poder listar,
              consultar su detalle, eliminar y cargar inventario por medio de un
              archivo.
            </Typography>
            <Typography gutterBottom variant="h5" component="h2">
              Listado de Inventario
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              component="p"
              className="styleMarginButtom"
            >
              La pantalla inicial tendrá la lista de los productos en el
              inventario que deben contar como mínimo con un nombre de producto,
              una cantidad, un costo, un precio y fecha desde la cual está
              disponible el producto. Cada línea debe tener la opción de
              visualizar su detalle, asi como tambien editar cantidad y costo o
              eliminar por completo la línea. El valor del precio será calculado
              con base en el costo, un margen de ganancia según el producto y un
              IVA del 19% y el precio no puede ser editado.{" "}
            </Typography>
            <Typography gutterBottom variant="h5" component="h2">
              Consulta de detalle
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              component="p"
              className="styleMarginButtom">           
              Al seleccionar alguno de los productos se presentara todo su
              detalle en una ventana, así como el precio unitario y el precio
              total teniendo en cuenta la cantidad.
            </Typography>
            <Typography gutterBottom variant="h5" component="h2">
              Proceso de carga de inventario
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              className="styleMarginButtom"
            >
              Por pantalla se debe poder realizar la carga de un csv con
              información de inventario de varios productos que contiene nombre
              del producto, tipo de producto, fecha desde que está disponible el
              producto, costo, cantidad y otros campos que considere puedan ser
              importantes para un inventario.
              <ul>
                <li>
                  Debe validar los campos (formato,longitud, caracteres
                  especiales, etc.)
                </li>
                <li>
                  El margen de ganancia por tipo de producto está dado por la
                  siguiente tabla:
                </li>
              </ul>
              <TableContainer>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">Producto</TableCell>
                      <TableCell align="center">Comisión</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map(row => (
                      <TableRow>
                        <TableCell align="center">{row.number}</TableCell>
                        <TableCell align="center">{row.value}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  }
}

export default TestDescApp;
