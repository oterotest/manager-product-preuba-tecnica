import React, { Component } from "react";
import "./../App.css";

import ProductList from "../service/ProducList";
import LoadFileApp from "./LoadFileApp";

class MainApp extends Component {

 
  render() {
    return (
      <div>
        <ProductList />
        <LoadFileApp />
      </div>
    );
  }
}

export default MainApp;
