import React, { Component } from "react";
import "./App.css";
import FooterApp from "./components/FooterApp";
import HeaderApp from "./components/HeaderApp";
import NavApp from "./components/NavApp";

class App extends Component {
 
  render() {
    return (
      <div className="style">
        <nav className="styleHeader">
          <HeaderApp />
        </nav>
        <main className="styleCenterContenedor">
          <NavApp />
        </main>
        <footer>
          <FooterApp />
        </footer>
      </div>
    );
  }
}

export default App;
